#+TITLE: April 2-15, 2022 (14 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 14
  :SPRINTSTART: <2022-04-02 Sat>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :END:
** akshay196
*** DONE Programming Kubernetes - Part VI
    CLOSED: [2022-04-10 Sun 21:45]
    :PROPERTIES:
    :ESTIMATED:   9
    :ACTUAL:   6.07
    :OWNER: akshay196
    :ID: READ.1637477677
    :TASKID: READ.1637477677
    :END:
    :LOGBOOK:
    CLOCK: [2022-04-10 Sun 21:00]--[2022-04-10 Sun 21:45] =>  0:45
    CLOCK: [2022-04-07 Thu 21:02]--[2022-04-07 Thu 21:34] =>  0:32
    CLOCK: [2022-04-06 Wed 20:02]--[2022-04-06 Wed 21:00] =>  0:58
    CLOCK: [2022-04-05 Tue 20:12]--[2022-04-05 Tue 21:25] =>  1:13
    CLOCK: [2022-04-04 Mon 20:50]--[2022-04-04 Mon 21:48] =>  0:58
    CLOCK: [2022-04-03 Sun 20:25]--[2022-04-03 Sun 20:57] =>  0:32
    CLOCK: [2022-04-02 Sat 19:55]--[2022-04-02 Sat 21:01] =>  1:06
    :END:
    - [X] Chapter 6. Solutions for Writing Operators     (4h)
    - [X] Chapter 7. Shipping Controllers and Operators  (5h)
*** DONE Watch Fosdem 2022 videos - Part II
    CLOSED: [2022-04-15 Fri 21:48]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   2.35
    :OWNER: akshay196
    :ID: READ.1645157068
    :TASKID: READ.1645157068
    :END:
    :LOGBOOK:
    CLOCK: [2022-04-15 Fri 20:50]--[2022-04-15 Fri 21:48] =>  0:58
    CLOCK: [2022-04-15 Fri 14:58]--[2022-04-15 Fri 15:28] =>  0:30
    CLOCK: [2022-04-15 Fri 11:35]--[2022-04-15 Fri 11:54] =>  0:19
    CLOCK: [2022-04-11 Mon 22:15]--[2022-04-11 Mon 22:49] =>  0:34
    :END:
    - [X]  [[https://fosdem.org/2022/schedule/event/postgresql_exploring_linux_memory_usage_and_io_performance_for_cloud_native_databases/][Exploring Linux Memory Usage and IO Performance for Cloud Native Databases]]                                         (45m)
    - [X]  [[https://fosdem.org/2022/schedule/event/python_concurrency_in_webapps/][Handling Concurrency in Web Application How *not* to build a URL Shortener]]                                         (45m)
    - [X]  [[https://fosdem.org/2022/schedule/event/python_unicode/][Messing with unicode A few possible attacks with unicode]]                                                           (45m)
** bhavin192
*** DONE Add StatefulSet scale example to fabric8 K8s
    CLOSED: [2022-04-08 Fri 21:53]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   2.02
    :OWNER:    bhavin192
    :ID:       DEV.1650019937
    :TASKID:   DEV.1650019937
    :END:
    :LOGBOOK:
    CLOCK: [2022-04-08 Fri 21:48]--[2022-04-08 Fri 21:53] =>  0:05
    CLOCK: [2022-04-08 Fri 21:01]--[2022-04-08 Fri 21:08] =>  0:07
    CLOCK: [2022-04-07 Thu 20:23]--[2022-04-07 Thu 20:59] =>  0:36
    CLOCK: [2022-04-06 Wed 22:20]--[2022-04-06 Wed 22:53] =>  0:33
    CLOCK: [2022-04-06 Wed 20:45]--[2022-04-06 Wed 21:25] =>  0:40
    :END:
    - https://github.com/fabric8io/kubernetes-client/issues/3948
*** DONE Watch FOSDEM 2022 - Containers [15/15]
    CLOSED: [2022-04-12 Tue 22:19]
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   6.38
    :OWNER:    bhavin192
    :ID:       READ.1650009933
    :TASKID:   READ.1650009933
    :END:
    :LOGBOOK:
    CLOCK: [2022-04-12 Tue 21:56]--[2022-04-12 Tue 22:19] =>  0:23
    CLOCK: [2022-04-12 Tue 21:06]--[2022-04-12 Tue 21:27] =>  0:21
    CLOCK: [2022-04-11 Mon 22:10]--[2022-04-11 Mon 22:32] =>  0:22
    CLOCK: [2022-04-11 Mon 20:34]--[2022-04-11 Mon 21:04] =>  0:30
    CLOCK: [2022-04-10 Sun 22:03]--[2022-04-10 Sun 22:23] =>  0:20
    CLOCK: [2022-04-10 Sun 19:33]--[2022-04-10 Sun 19:52] =>  0:19
    CLOCK: [2022-04-10 Sun 19:24]--[2022-04-10 Sun 19:27] =>  0:03
    CLOCK: [2022-04-10 Sun 17:33]--[2022-04-10 Sun 18:31] =>  0:58
    CLOCK: [2022-04-09 Sat 22:49]--[2022-04-09 Sat 23:00] =>  0:11
    CLOCK: [2022-04-05 Tue 20:16]--[2022-04-05 Tue 21:16] =>  1:00
    CLOCK: [2022-04-04 Mon 20:20]--[2022-04-04 Mon 21:23] =>  1:03
    CLOCK: [2022-04-03 Sun 19:02]--[2022-04-03 Sun 19:55] =>  0:53
    :END:
    - [X] [[https://fosdem.org/2022/schedule/event/container_boot2container/][Boot2container: An initramfs for reproducible infrastructures]]         (30m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_debugging/][Debugging in containers]]                                               (30m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_distributed_storage/][Distributed Storage in the Cloud]]                                      (30m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_k8s_webassembly/][Extending Kubernetes with WebAssembly]]                                 (15m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_k8s_disappear/][Freedom Means That Kubernetes Needs To Disappear]]                      (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_harbor/][Harbor - The Container Registry]]                                       (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_flatcar_autoupdate/][How I learned to stop worrying and love Flatcar’s auto-update]]         (30m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_k8gb_balancer/][A cloud native Kubernetes Global Balancer]]                             (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_ipfs_image/][P2P Container Image Distribution on IPFS With Containerd and Nerdctl]]  (25m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_redpak/][Redpak: Ultra light weight container for embedded systems]]             (25m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_k8s_mysql/][Solutions for running MySQL in Kubernetes]]                             (30m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_userlan_tcpip/][Userland TCP/IP stack for external container connectivity]]             (25m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_fat_layers/][What made your container fat?]] ([[https://www.youtube.com/watch?v=5HspfvshAZ4][YouTube]])                               (05m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_notebook_images/][An easy and elegant way to manage and build your notebook images]]      (25m)
    - [X] [[https://fosdem.org/2022/schedule/event/container_singularity_apptainer/][From Singularity to Apptainer]] ([[https://www.youtube.com/watch?v=elC6T60VgzM][YouTube]])                               (45m)
*** DONE Watch FOSDEM 2022 - Monitoring and Observability - Part I [7/7]
    CLOSED: [2022-04-15 Fri 22:41]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   4.20
    :OWNER:    bhavin192
    :ID:       READ.1650009933
    :TASKID:   READ.1650009933
    :END:
    :LOGBOOK:
    CLOCK: [2022-04-15 Fri 22:20]--[2022-04-15 Fri 22:41] =>  0:21
    CLOCK: [2022-04-15 Fri 20:26]--[2022-04-15 Fri 21:10] =>  0:44
    CLOCK: [2022-04-15 Fri 19:08]--[2022-04-15 Fri 19:26] =>  0:18
    CLOCK: [2022-04-15 Fri 18:38]--[2022-04-15 Fri 18:46] =>  0:08
    CLOCK: [2022-04-15 Fri 16:37]--[2022-04-15 Fri 18:03] =>  1:26
    CLOCK: [2022-04-14 Thu 22:41]--[2022-04-14 Thu 22:56] =>  0:15
    CLOCK: [2022-04-13 Wed 21:35]--[2022-04-13 Wed 21:41] =>  0:06
    CLOCK: [2022-04-13 Wed 20:34]--[2022-04-13 Wed 21:28] =>  0:54
    :END:
    - [X] [[https://fosdem.org/2022/schedule/event/adapting_otel/][Adopting OpenTelemetry and its collector]]                      (45m)
    - [X] [[https://fosdem.org/2022/schedule/event/multi_dc_cloud_native_observability/][Bootstrapping a multi dc cloud native observability stack]]     (45m)
    - [X] [[https://fosdem.org/2022/schedule/event/pyroscope/][Introduction to Continuous Profiling using Pyroscope]]          (45m)
    - [X] [[https://fosdem.org/2022/schedule/event/monitoring_kafka_using_ebpf/][Monitoring Kafka without instrumentation using eBPF]]           (45m)
    - [X] [[https://fosdem.org/2022/schedule/event/periskop/][Periskop: Exception Monitoring at Scale]]                       (45m)
    - [X] [[https://fosdem.org/2022/schedule/event/cloud_native_profiling/][Profiling in the cloud-native era]]                             (45m)
    - [X] [[https://fosdem.org/2022/schedule/event/unikraft/][Unikraft Performance Monitoring with Prometheus]] ([[https://www.youtube.com/watch?v=hBiq2xjwECA][YouTube]])     (20m)
