#+TITLE: March 18-April 1, 2022 (15 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 15
  :SPRINTSTART: <2022-04-18 Fri>
  :wpd-bhavin192: 1
  :END:
** nihaal
*** DONE Read Linux Weekly News [2/2]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.41
    :OWNER: nihaal
    :ID: READ.1647697149
    :TASKID: READ.1647697149
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-31 Thu 16:59]--[2022-03-31 Thu 17:34] =>  0:35
    CLOCK: [2022-03-27 Sun 12:49]--[2022-03-27 Sun 13:07] =>  0:18
    CLOCK: [2022-03-27 Sun 11:16]--[2022-03-27 Sun 11:48] =>  0:32
    :END:
    - [X] Weekly edition for March 17, 2021
    - [X] Weekly edition for March 24, 2021
*** Write post on Syzkaller
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:
    :OWNER: nihaal
    :ID: WRITE.1647697225
    :TASKID: WRITE.1647697225
    :END:
*** Write post on early userspace
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:
    :OWNER: nihaal
    :ID: WRITE.1647697551
    :TASKID: WRITE.1647697551
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-19 Sat 20:56]--[2022-03-19 Sat 21:25] =>  0:29
    :END:
*** Write post on ftrace
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:
    :OWNER: nihaal
    :ID: WRITE.1647697527
    :TASKID: WRITE.1647697527
    :END:
*** DONE Read about Dirty COW and Dirty pipe vulnerabilities [2/2]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   0.87
    :OWNER: nihaal
    :ID: READ.1647697583
    :TASKID: READ.1647697583
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-22 Tue 19:44]--[2022-03-22 Tue 20:36] =>  0:52
    :END:
    - [X] Dirty COW
      - [X] https://lwn.net/Articles/849638/
      - [X] https://lwn.net/Articles/849876/
    - [X] Dirty Pipe
      - [X] https://dirtypipe.cm4all.com/
** bhavin192
*** DONE Watch FOSDEM 2022 - Go [9/9]
    CLOSED: [2022-03-24 Thu 20:21]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   5.00
    :OWNER:    bhavin192
    :ID:       READ.1650009933
    :TASKID:   READ.1650009933
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-24 Thu 19:52]--[2022-03-24 Thu 20:21] =>  0:29
    CLOCK: [2022-03-24 Thu 17:11]--[2022-03-24 Thu 17:45] =>  0:34
    CLOCK: [2022-03-23 Wed 20:22]--[2022-03-23 Wed 21:36] =>  1:14
    CLOCK: [2022-03-22 Tue 22:35]--[2022-03-22 Tue 22:41] =>  0:06
    CLOCK: [2022-03-22 Tue 21:20]--[2022-03-22 Tue 21:31] =>  0:11
    CLOCK: [2022-03-22 Tue 20:20]--[2022-03-22 Tue 21:16] =>  0:56
    CLOCK: [2022-03-21 Mon 20:18]--[2022-03-21 Mon 21:25] =>  1:07
    CLOCK: [2022-03-20 Sun 22:28]--[2022-03-20 Sun 22:37] =>  0:09
    CLOCK: [2022-03-19 Sat 22:39]--[2022-03-19 Sat 22:48] =>  0:09
    CLOCK: [2022-03-18 Fri 22:28]--[2022-03-18 Fri 22:33] =>  0:05
    :END:
    - [X] [[https://fosdem.org/2022/schedule/event/go_welcome/][Welcome to the Go Devroom]]                                     (20m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_slices_maps_channels/][Dissecting Slices, Maps and Channels in Go]]                    (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_errors/][Mastering Your Error Domain]]                                   (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_tinygo_wifi/][Go Further Without Wires]]                                      (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_finite_automata/][Fun with Finite Automata]]                                      (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_log4shell/][Fuzzy generics]]                                                (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_json/][JSON Serialization - The Fine Print]]                           (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_why_embedded/][Why your next embedded project should be written in Go]]        (35m)
    - [X] [[https://fosdem.org/2022/schedule/event/go_state_of_go/][The State of Go]]                                               (35m)
