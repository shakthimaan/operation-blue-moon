#+TITLE: May 24, 2020 - June 4, 2020 (12 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 12
  :SPRINTSTART: <2020-05-24 Sun>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :END:
** akshay196
*** DONE The Go Programming Language - Part II
    CLOSED: [2020-06-04 Thu 22:08]
    :PROPERTIES:
    :ESTIMATED:  12
    :ACTUAL:   10.08
    :OWNER: akshay196
    :ID: READ.1587566538
    :TASKID: READ.1587566538
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-04 Thu 21:08]--[2020-06-04 Thu 22:07] =>  0:59
    CLOCK: [2020-06-04 Thu 07:25]--[2020-06-04 Thu 08:17] =>  0:52
    CLOCK: [2020-06-03 Wed 07:44]--[2020-06-03 Wed 09:41] =>  1:57
    CLOCK: [2020-06-02 Tue 05:49]--[2020-06-02 Tue 07:06] =>  1:17
    CLOCK: [2020-06-01 Mon 06:38]--[2020-06-01 Mon 07:17] =>  0:39
    CLOCK: [2020-05-31 Sun 08:39]--[2020-05-31 Sun 09:26] =>  0:47
    CLOCK: [2020-05-30 Sat 05:48]--[2020-05-30 Sat 06:16] =>  0:28
    CLOCK: [2020-05-29 Fri 06:29]--[2020-05-29 Fri 07:09] =>  0:40
    CLOCK: [2020-05-28 Thu 05:39]--[2020-05-28 Thu 06:23] =>  0:44
    CLOCK: [2020-05-26 Tue 19:29]--[2020-05-26 Tue 20:49] =>  1:20
    CLOCK: [2020-05-25 Mon 19:30]--[2020-05-25 Mon 19:52] =>  0:22
    :END:
    - [X] Chapter 6.1. Method Declarations                                  ( 60m)
    - [X] Chapter 6.2. Methods with a Pointer Receiver                      (120m)
    - [X] Chapter 6.3. Composing Types by Struct Embedding                  ( 60m)
    - [X] Chapter 6.4. Method Values and Expressions                        ( 30m)
    - [X] Chapter 6.5. Example: Bit Vector Type                             (120m)
    - [X] Chapter 6.6. Encapsulation                                        ( 60m)
    - [X] Chapter 7.   Interfaces                                           ( 15m)
    - [X] Chapter 7.1. Interfaces as Contracts                              ( 60m)
    - [X] Chapter 7.2. Interface Types                                      ( 60m)
    - [X] Chapter 7.3. Interface Satisfaction                               ( 60m)
    - [X] Chapter 7.4. Parsing Flags with flag.Value                        ( 90m)
** bhavin192
*** DONE Programming Kubernetes - 2. Kubernetes API Basics
    CLOSED: [2020-05-28 Thu 20:48]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   2.30
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-28 Thu 20:25]--[2020-05-28 Thu 20:48] =>  0:23
    CLOCK: [2020-05-27 Wed 21:03]--[2020-05-27 Wed 21:29] =>  0:26
    CLOCK: [2020-05-27 Wed 19:49]--[2020-05-27 Wed 20:25] =>  0:36
    CLOCK: [2020-05-25 Mon 22:49]--[2020-05-25 Mon 22:57] =>  0:08
    CLOCK: [2020-05-25 Mon 22:17]--[2020-05-25 Mon 22:44] =>  0:27
    CLOCK: [2020-05-25 Mon 20:57]--[2020-05-25 Mon 21:15] =>  0:18
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/
*** DONE Programming Kubernetes - 3. Basics of client-go
    CLOSED: [2020-06-04 Thu 23:13]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   6.57
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-04 Thu 22:16]--[2020-06-04 Thu 23:16] =>  1:00
    CLOCK: [2020-06-04 Thu 20:32]--[2020-06-04 Thu 21:03] =>  0:31
    CLOCK: [2020-06-04 Thu 19:39]--[2020-06-04 Thu 20:09] =>  0:30
    CLOCK: [2020-06-03 Wed 21:58]--[2020-06-03 Wed 23:16] =>  1:18
    CLOCK: [2020-06-02 Tue 22:21]--[2020-06-02 Tue 23:34] =>  1:13
    CLOCK: [2020-06-01 Mon 21:31]--[2020-06-01 Mon 21:46] =>  0:15
    CLOCK: [2020-06-01 Mon 19:29]--[2020-06-01 Mon 20:32] =>  1:03
    CLOCK: [2020-05-28 Thu 22:17]--[2020-05-28 Thu 22:41] =>  0:24
    CLOCK: [2020-05-28 Thu 20:52]--[2020-05-28 Thu 21:12] =>  0:20
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/

** gandalfdwite
*** DONE Blog post on Kubernetes Power tools [1/1]
    CLOSED: [2020-05-27 Wed 16:12]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   3.08
    :OWNER:    gandalfdwite
    :ID:       WRITE.1589595983
    :TASKID:   WRITE.1589595983
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-24 Sun 20:19]--[2020-05-24 Sun 22:19] =>  2:00
    CLOCK: [2020-05-25 Mon 22:00]--[2020-05-25 Mon 23:05] =>  1:05
    :END:
    - [X] Write blog post on K8s power tools ( 3h )
*** DONE Training on Azure DevOps [1/1]
    CLOSED: [2020-06-08 Mon 16:12]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   3.00
    :OWNER: gandalfdwite
    :ID: EVENT.1589728251
    :TASKID: EVENT.1589728251
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-04 Thu 17:30]--[2020-06-04 Thu 20:30] =>  3:00
    :END:
    - [X] Attend Training on Azure DevOps  ( 3h )
*** DONE Prometheus: Up & Running - Part III [2/2]
    CLOSED: [2020-06-02 Tue 16:10]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   6.02
    :OWNER: gandalfdwite
    :ID: READ.1587476760
    :TASKID: READ.1587476760
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-30 Sat 11:05]--[2020-05-30 Sat 12:30] =>  1:25
    CLOCK: [2020-05-29 Fri 23:20]--[2020-05-30 Sat 00:30] =>  1:10
    CLOCK: [2020-05-28 Thu 20:30]--[2020-05-28 Thu 21:30] =>  1:00
    CLOCK: [2020-05-27 Wed 23:05]--[2020-05-28 Thu 00:30] =>  1:25
    CLOCK: [2020-05-26 Tue 18:17]--[2020-05-26 Tue 19:18] =>  1:01
    :END:
    - [X] 12. Writing exporters                               ( 3h )
    - [X] 13. Intro to PromQL                                 ( 3h )
