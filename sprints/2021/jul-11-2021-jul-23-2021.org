#+TITLE: July 11-23, 2021 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2021-07-11 Sun>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :END:
** akshay196
*** DONE Read Kubernetes docs - Part II
    CLOSED: [2021-07-22 Thu 16:57]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   8.25
    :OWNER:    akshay196
    :ID:       READ.1623432379
    :TASKID:   READ.1623432379
    :END:
    :LOGBOOK:
    CLOCK: [2021-07-22 Thu 16:11]--[2021-07-22 Thu 16:57] =>  0:46
    CLOCK: [2021-07-21 Wed 23:14]--[2021-07-22 Thu 00:07] =>  0:53
    CLOCK: [2021-07-19 Mon 19:20]--[2021-07-19 Mon 20:24] =>  1:04
    CLOCK: [2021-07-18 Sun 17:58]--[2021-07-18 Sun 18:42] =>  0:44
    CLOCK: [2021-07-17 Sat 18:46]--[2021-07-17 Sat 19:32] =>  0:46
    CLOCK: [2021-07-16 Fri 22:56]--[2021-07-16 Fri 23:50] =>  0:54
    CLOCK: [2021-07-15 Thu 21:56]--[2021-07-15 Thu 22:23] =>  0:27
    CLOCK: [2021-07-14 Wed 21:44]--[2021-07-14 Wed 22:17] =>  0:33
    CLOCK: [2021-07-13 Tue 20:24]--[2021-07-13 Tue 21:21] =>  0:57
    CLOCK: [2021-07-12 Mon 19:21]--[2021-07-12 Mon 20:32] =>  1:11
    :END:
    https://kubernetes.io/docs
    - [X] Workloads                              (90m)
    - [X] Service                                (90m)
    - [X] DNS for Services and Pods              (90m)
    - [X] Connecting Applications with Services  (90m)
** bhavin192
*** DONE Write blog post about RepRap 3D printer first build
    CLOSED: [2021-07-18 Sun 14:02]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   8.00
    :OWNER:    bhavin192
    :ID:       WRITE.1626025082
    :TASKID:   WRITE.1626025082
    :END:
    :LOGBOOK:
    CLOCK: [2021-07-18 Sun 13:31]--[2021-07-18 Sun 14:02] =>  0:31
    CLOCK: [2021-07-18 Sun 13:11]--[2021-07-18 Sun 13:18] =>  0:07
    CLOCK: [2021-07-17 Sat 21:52]--[2021-07-17 Sat 23:30] =>  1:38
    CLOCK: [2021-07-17 Sat 20:42]--[2021-07-17 Sat 21:02] =>  0:20
    CLOCK: [2021-07-16 Fri 23:12]--[2021-07-16 Fri 23:24] =>  0:12
    CLOCK: [2021-07-15 Thu 21:05]--[2021-07-15 Thu 21:16] =>  0:11
    CLOCK: [2021-07-15 Thu 20:08]--[2021-07-15 Thu 20:59] =>  0:51
    CLOCK: [2021-07-14 Wed 20:53]--[2021-07-14 Wed 21:13] =>  0:20
    CLOCK: [2021-07-14 Wed 20:13]--[2021-07-14 Wed 20:53] =>  0:40
    CLOCK: [2021-07-13 Tue 20:29]--[2021-07-13 Tue 21:05] =>  0:36
    CLOCK: [2021-07-13 Tue 20:00]--[2021-07-13 Tue 20:26] =>  0:26
    CLOCK: [2021-07-12 Mon 20:24]--[2021-07-12 Mon 21:24] =>  1:00
    CLOCK: [2021-07-11 Sun 22:33]--[2021-07-11 Sun 23:05] =>  0:32
    CLOCK: [2021-07-11 Sun 22:13]--[2021-07-11 Sun 22:32] =>  0:19
    CLOCK: [2021-07-11 Sun 17:59]--[2021-07-11 Sun 18:16] =>  0:17
    :END:
