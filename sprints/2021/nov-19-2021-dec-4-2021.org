#+TITLE: November 19, 2021 - December 4, 2021 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2021-11-19 Fri>
  :wpd-nihaal: 1
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :END:
** akshay196
*** DONE Let's Go Book - Part VI
    CLOSED: [2021-12-01 Wed 20:39]
    :PROPERTIES:
    :ESTIMATED: 9
    :ACTUAL:   4.83
    :OWNER:    akshay196
    :ID:       READ.1629798238
    :TASKID:   READ.1629798238
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-01 Wed 19:42]--[2021-12-01 Wed 20:39] =>  0:57
    CLOCK: [2021-11-30 Tue 22:43]--[2021-11-30 Tue 23:18] =>  0:35
    CLOCK: [2021-11-29 Mon 19:42]--[2021-11-29 Mon 19:59] =>  0:17
    CLOCK: [2021-11-28 Sun 19:35]--[2021-11-28 Sun 19:36] =>  0:01
    CLOCK: [2021-11-24 Wed 22:15]--[2021-11-24 Wed 22:46] =>  0:31
    CLOCK: [2021-11-23 Tue 13:11]--[2021-11-23 Tue 14:11] =>  1:00
    CLOCK: [2021-11-21 Sun 12:29]--[2021-11-21 Sun 13:58] =>  1:29
    :END:
    - [X] 7.1. Installing a Router             (1h)
    - [X] 7.2. Implementing RESTful Routes     (1h)
    - [X] 8.1. Setting Up a Form               (1h)
    - [X] 8.2. Parsing Form Data               (1h)
    - [X] 8.3. Data Validation                 (1h)
    - [X] 8.4. Scaling Data Validation         (1h)
    - [X] 9.1. Installing a Session Manager    (1h)
    - [X] 9.2. Setting Up the Session Manager  (1h)
    - [X] 9.3. Working with Session Data       (1h)
*** DONE Programming Kubernetes - Part I
    CLOSED: [2021-12-04 Sat 21:36]
    :PROPERTIES:
    :ESTIMATED: 2.5
    :ACTUAL:   3.02
    :OWNER: akshay196
    :ID: READ.1637477677
    :TASKID: READ.1637477677
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-04 Sat 20:41]--[2021-12-04 Sat 21:36] =>  0:55
    CLOCK: [2021-12-03 Fri 22:34]--[2021-12-03 Fri 22:59] =>  0:25
    CLOCK: [2021-12-02 Thu 23:20]--[2021-12-02 Thu 23:55] =>  0:35
    CLOCK: [2021-12-02 Thu 12:27]--[2021-12-02 Thu 13:33] =>  1:06
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/
    - [X] Chapter 1. Introduction                  (2.5h)
** bhavin192
*** DONE Help with EmacsConf 2021 video captions - Part II
    CLOSED: [2021-11-28 Sun 19:23]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   10.85
    :OWNER:    bhavin192
    :ID:       EVENT.1636907342
    :TASKID:   EVENT.1636907342
    :END:
    :LOGBOOK:
    CLOCK: [2021-11-28 Sun 17:49]--[2021-11-28 Sun 19:23] =>  1:34
    CLOCK: [2021-11-28 Sun 13:41]--[2021-11-28 Sun 14:37] =>  0:56
    CLOCK: [2021-11-28 Sun 13:26]--[2021-11-28 Sun 13:29] =>  0:03
    CLOCK: [2021-11-27 Sat 15:31]--[2021-11-27 Sat 16:32] =>  1:01
    CLOCK: [2021-11-27 Sat 00:39]--[2021-11-27 Sat 00:45] =>  0:06
    CLOCK: [2021-11-25 Thu 23:21]--[2021-11-25 Thu 23:28] =>  0:07
    CLOCK: [2021-11-25 Thu 22:09]--[2021-11-25 Thu 22:30] =>  0:21
    CLOCK: [2021-11-25 Thu 21:53]--[2021-11-25 Thu 22:07] =>  0:14
    CLOCK: [2021-11-23 Tue 23:23]--[2021-11-23 Tue 23:33] =>  0:10
    CLOCK: [2021-11-23 Tue 22:26]--[2021-11-23 Tue 23:23] =>  0:57
    CLOCK: [2021-11-23 Tue 20:47]--[2021-11-23 Tue 21:04] =>  0:17
    CLOCK: [2021-11-22 Mon 22:16]--[2021-11-22 Mon 23:17] =>  1:01
    CLOCK: [2021-11-22 Mon 22:11]--[2021-11-22 Mon 22:13] =>  0:02
    CLOCK: [2021-11-21 Sun 17:22]--[2021-11-21 Sun 17:27] =>  0:05
    CLOCK: [2021-11-21 Sun 17:09]--[2021-11-21 Sun 17:20] =>  0:11
    CLOCK: [2021-11-21 Sun 16:47]--[2021-11-21 Sun 17:01] =>  0:14
    CLOCK: [2021-11-21 Sun 15:29]--[2021-11-21 Sun 16:21] =>  0:52
    CLOCK: [2021-11-21 Sun 13:48]--[2021-11-21 Sun 14:14] =>  0:26
    CLOCK: [2021-11-20 Sat 22:31]--[2021-11-20 Sat 22:42] =>  0:11
    CLOCK: [2021-11-20 Sat 18:50]--[2021-11-20 Sat 18:56] =>  0:06
    CLOCK: [2021-11-20 Sat 17:22]--[2021-11-20 Sat 17:40] =>  0:18
    CLOCK: [2021-11-20 Sat 16:43]--[2021-11-20 Sat 17:16] =>  0:33
    CLOCK: [2021-11-20 Sat 13:35]--[2021-11-20 Sat 13:48] =>  0:13
    CLOCK: [2021-11-20 Sat 12:34]--[2021-11-20 Sat 13:22] =>  0:48
    CLOCK: [2021-11-19 Fri 22:14]--[2021-11-19 Fri 22:19] =>  0:05
    :END:
    - https://emacsconf.org/2021/contribute/
*** DONE Attend EmacsConf 2021
    CLOSED: [2021-11-29 Mon 22:41]
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:   6.23
    :OWNER:    bhavin192
    :ID:       READ.1642527564
    :TASKID:   READ.1642527564
    :END:
    :LOGBOOK:
    CLOCK: [2021-11-29 Mon 21:43]--[2021-11-29 Mon 22:41] =>  0:58
    CLOCK: [2021-11-28 Sun 19:30]--[2021-11-29 Mon 00:16] =>  4:46
    CLOCK: [2021-11-28 Sun 11:50]--[2021-11-28 Sun 12:20] =>  0:30
    :END:
** nihaal
*** DONE Write blog posts
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   12.97
    :OWNER: nihaal
    :ID: WRITE.1637395336
    :TASKID: WRITE.1637395336
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-04 Sat 22:40]--[2021-12-05 Sun 01:05] =>  2:25
    CLOCK: [2021-12-04 Sat 18:14]--[2021-12-04 Sat 21:17] =>  3:03
    CLOCK: [2021-11-29 Mon 19:01]--[2021-11-29 Mon 21:51] =>  2:50
    CLOCK: [2021-11-28 Sun 20:49]--[2021-11-28 Sun 22:48] =>  1:59
    CLOCK: [2021-11-23 Tue 18:51]--[2021-11-23 Tue 19:57] =>  1:06
    CLOCK: [2021-11-22 Mon 19:00]--[2021-11-22 Mon 20:00] =>  1:00
    CLOCK: [2021-11-21 Sun 19:50]--[2021-11-21 Sun 20:25] =>  0:35
    :END:
    - [X] Creating Sysfs files                        (2h)
    - [X] FOSSHack 21                                 (3h)
    - [X] Publishing to PyPI                          (2h)
*** DONE Eudyptula Challenge - Part III
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   6.72
    :OWNER: nihaal
    :ID: DEV.1632240155
    :TASKID: DEV.1632240155
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-03 Fri 22:08]--[2021-12-03 Fri 22:25] =>  0:17
    CLOCK: [2021-12-03 Fri 20:22]--[2021-12-03 Fri 21:19] =>  0:57
    CLOCK: [2021-12-03 Fri 18:22]--[2021-12-03 Fri 19:55] =>  1:33
    CLOCK: [2021-12-01 Wed 20:30]--[2021-12-01 Wed 21:10] =>  0:40
    CLOCK: [2021-12-01 Wed 19:08]--[2021-12-01 Wed 20:08] =>  1:00
    CLOCK: [2021-11-30 Tue 20:32]--[2021-11-30 Tue 21:28] =>  0:56
    CLOCK: [2021-11-30 Tue 19:42]--[2021-11-30 Tue 19:57] =>  0:15
    CLOCK: [2021-11-24 Wed 18:56]--[2021-11-24 Wed 20:01] =>  1:05
    :END:
    - [X] 10. Send a checkpatch fix                   (3h)
    - [X] 11. Add a sysfs entry to an existing driver (2h)
*** DONE Read Linux Weekly News
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.42
    :OWNER: nihaal
    :ID: READ.1637395601
    :TASKID: READ.1637395601
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-02 Thu 19:15]--[2021-12-02 Thu 20:02] =>  0:47
    CLOCK: [2021-12-02 Thu 18:27]--[2021-12-02 Thu 19:05] =>  0:38
    :END:
    - [X] Weekly edition for November 18, 2021        (1h)
    - +[ ] Weekly edition for November 25, 2021       (1h)+
   
