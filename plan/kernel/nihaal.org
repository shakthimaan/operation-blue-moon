#+AUTHOR: Abdun Nihaal
#+EMAIL: nihaal.an@gmail.com
#+TAGS: read write dev ops event meeting
* GOALS
** Linux Kernel
*** Read
**** Linux Device Drivers, 3rd edition [12/18]
     - [X] 1. An Introduction to Device Drivers
     - [X] 2. Building and Running Modules
     - [X] 3. Char Drivers
     - [X] 4. Debugging Techniques
     - [X] 5. Concurrency and Race Conditions
     - [X] 6. Advanced Char Driver Operations
     - [X] 7. Time, Delays, and Deferred Work
     - [X] 8. Allocating Memory
     - [X] 9. Communicating with Hardware
     - [X] 10. Interrupt Handling
     - [X] 11. Data Types in the Kernel
     - [X] 12. PCI Drivers
     - [ ] 13. USB Drivers
     - [ ] 14. The Linux Device Model
     - [ ] 15. Memory Mapping and DMA
     - [ ] 16. Block Drivers
     - [ ] 17. Network Drivers
     - [ ] 18. TTY Drivers
*** Finish Eudyptula Challenge [17/20] 
    The task descriptions can be found [[https://github.com/agelastic/eudyptula][here]].
    - [X] 1. Hello World Kernel module
    - [X] 2. Building the kernel
    - [X] 3. Patch the Makefile to rename the kernel
    - +[ ] 4. Kernel coding style+
    - [X] 5. USB keyboard driver
    - [X] 6. Misc Character driver
    - [X] 7. Compile and run Linux-next kernel
    - [X] 8. Creating debugfs entries
    - [X] 9. Creating sysfs entries
    - [X] 10. Send a checkpatch fix
    - [X] 11. Add a sysfs entry to an existing driver
    - [X] 12. Working with Linked Lists
    - [X] 13. Slab cache
    - [X] 14. Creating procfs entries
    - [X] 15. Create a new system call
    - [X] 16. Using Sparse tool
    - [X] 17. Create a kernel thread
    - [X] 18. Working with queues
    - [ ] 19. Write a netfilter module
    - [ ] 20. Working with ioctls and filesystems
** Write blog
* PLAN
